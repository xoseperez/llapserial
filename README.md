# LLAPSerial

LLAP Protocol Implementation Library for Arduino.
See the related post in my blog [Ciseco XRF modules and LLAP Protocol][1].

Originally based on [Ciseco's LLAPSerial library][2].

## Main changes over Ciseco version are:

* Removed power management code (this library focuses on LLAP protocol and messaging)
* Added support to use different Hardware and Software serial port
* Provided a unique overloaded sendMessage method that supports sending char/int/float messages
* Provided a way to broadcast messages (see below)
* Defined the "coordinator" node, which will always process all messages, regardless the destination
* Disallow CHDEVID to coordinator nodes
* Major renaming and refactoring (sorry)
* Added doc comments
* Address allocation and persistence functionality

The LLAP Implementation of this version is 100% backwards compatible with the original one.
The only difference protocol-wise is that I have added the possibility of sending
broadcast messages using '..' as the destination ID. These messages will be processed by 
all nodes regardless their ID or role. 
This is intended for a coordinator to send the same message to all other nodes,
for instance a timestamp to sync them all.
If you don't use this feature you should be able to 
have devices using both libraries communicating between them.

The library also supports address negotiation (STARTED, ACK, CHDEVID and REBOOT) and persistence.
Address negotiation is done in the begin() method, so you must call it before any other thing.
Address management resembles DHCP, the coordinator manages a pool of addresses and provides addresses to nodes upon request.
The address pool implementation is very simple (it stores the highest value so far).
Address pool can be changed from the code (DYNAMIC_ADDRESSES_START and DYNAMIC_ADDRESSES values) and defaults to from BA to EZ (104 addresses).
Any node with static address should use an address outside the pool range to avoid collisions.
Nodes can persist their address in EEPROM, so the keep the same address after reboot.
Coordinator issues a REBOOT after a CHDEVID, as required by LLAP Reference Guide,
but the library will perform a soft-reboot in the device only if it's address is dynamic, i.e. nodes started with begin(false).

See the examples.

## Whys

### Why haven't you just forked the original repo instead of creating a new one?

Because even thou the protocol is 100% backwards compatible, the API of the library is not.
So it is not a drop-in replacement for Ciseco's one, you will have to modify your code in order to use my library.

### Why is your folders structure the way it is?

I use Ino Tookit to build my code (http://www.inotool.org). This tool enforces you to use a given structure for your projects,
the same way Arduino IDE does (the folder has to be named after the main .ino file).
So to use Ino while keeping compatibility with the Arduino way of publishing libraries I use this structure:

* root
    * COPYING 
    * README.md
    * code
        * src
            * .empty
        * lib
            * LLAPSerial
                * LLAPSerial.cpp
                * LLAPSerial.h
                * Examples
                    * LLAP_Coordinator
                        * LLAP_Coordinator.ino
                    * LLAP_DHT22
                        * LLAP_DHT22.ino
                    * LLAP_Node
                        * LLAP_Node.ino


So I just symlink code/lib/LLAPSerial folder from inside the Arduino libraries folder and the library and examples come available from the Arduino IDE.
To test the examples using Ino I symlink each example  to code/src (which is empty) and just do a "ino build".

### Why GPLv3?

The original library by Ciseco lacks a license statement.
I'm not very much into licenses, I just want my code to be freely available, just like the code I've learnt from.
GPLv3 is the license I've been using lately.

[1]: http://tinkerman.eldiariblau.net/ciseco-xrf-modules-and-llap-protocol/
[2]: https://github.com/CisecoPlc/LLAPSerial

